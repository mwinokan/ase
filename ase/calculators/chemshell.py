
# The ChemShell Calculator will go here

from ase.calculators.general import Calculator

import asemolplot as amp

import mout

class ChemShellQM(Calculator):

  filename_input_coords = "chsh_input.xyz"
  filename_output_dat = "chsh_output.dat"

  def __init__(self,loadEurekaModule=True):

    self.__name__ = "ChemShellQM"

    mout.headerOut(self.__name__+".__init__")

    # initialize the ase.calculators.general calculator
    Calculator.__init__(self)

    if loadEurekaModule:
      import module
      module.module("purge")
      module.module("load","intel/Intel_Parallel_Suite/xe_2019_5")

    # print(self.atoms)

    mout.headerOut("Done.")

  # def run():

  def update(self, atoms):
    self.calculate(atoms)

  def calculate(self,atoms):

    amp.write(self.filename_input_coords,atoms)

    import os
    import subprocess

    environment_dictionary = dict(os.environ)

    environment_dictionary['CHEMSH'] = "/users/mw00368/CHEMSHELL_intel/chemsh-py-20.0.0/bin/intel/chemsh.x"

    subprocess.Popen('mpirun -n 16 $CHEMSH gamess_spe.py',
                     shell=True,
                     env=environment_dictionary).wait()

    with open(self.filename_output_dat,"r") as input_dat:
      for line in input_dat:
        self.energy_zero = float(line.strip().split()[0])
        break

    print(self.energy_zero)
    